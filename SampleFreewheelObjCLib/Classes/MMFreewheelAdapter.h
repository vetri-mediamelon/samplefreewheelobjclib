//
//  MMFreewheelAdapter.h
//  Pods-SampleFreewheelObjCLib_Tests
//
//  Created by MacAir 1 on 18/09/20.
//

#import <Foundation/Foundation.h>
#import <AdManager/AdManager.h>

NS_ASSUME_NONNULL_BEGIN

@interface MMFreewheelAdapter : NSObject
@property (nonatomic, retain) id<FWContext> adContext;

-(void) setFreewheelAdContext: (id)fwContext;

@end

NS_ASSUME_NONNULL_END
