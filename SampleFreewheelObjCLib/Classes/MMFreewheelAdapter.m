//
//  MMFreewheelAdapter.m
//  Pods-SampleFreewheelObjCLib_Tests
//
//  Created by MacAir 1 on 18/09/20.
//

#import "MMFreewheelAdapter.h"
#import <AdManager/AdManager.h>
#import <AdManager/FWSDK.h>

@implementation MMFreewheelAdapter

-(void) setFreewheelAdContext:(id)fwContext {
    NSLog(@"Hola!!!, Successfully set Ad context");
    self.adContext = fwContext;
    
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(_onAdRequestCompleted:) name:FWRequestCompleteNotification object:self.adContext];
}

- (void)_onAdRequestCompleted:(NSNotification *)notification {
    NSLog(@"onAdRequestCompleted");
}

@end
