# SampleFreewheelObjCLib

[![CI Status](https://img.shields.io/travis/vetri/SampleFreewheelObjCLib.svg?style=flat)](https://travis-ci.org/vetri/SampleFreewheelObjCLib)
[![Version](https://img.shields.io/cocoapods/v/SampleFreewheelObjCLib.svg?style=flat)](https://cocoapods.org/pods/SampleFreewheelObjCLib)
[![License](https://img.shields.io/cocoapods/l/SampleFreewheelObjCLib.svg?style=flat)](https://cocoapods.org/pods/SampleFreewheelObjCLib)
[![Platform](https://img.shields.io/cocoapods/p/SampleFreewheelObjCLib.svg?style=flat)](https://cocoapods.org/pods/SampleFreewheelObjCLib)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

SampleFreewheelObjCLib is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'SampleFreewheelObjCLib'
```

## Author

vetri, vetri@mediamelon.com

## License

SampleFreewheelObjCLib is available under the MIT license. See the LICENSE file for more info.
