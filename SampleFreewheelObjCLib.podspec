#
# Be sure to run `pod lib lint SampleFreewheelObjCLib.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'SampleFreewheelObjCLib'
  s.version          = '0.1.0'
  s.summary          = 'Sample test lib for freewheel adpater'
  s.description      = 'Sample test lib for freewheel mediamelon objc adpater'

  s.homepage         = 'https://github.com/vetri/SampleFreewheelObjCLib'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'vetri' => 'vetri@mediamelon.com' }
  s.source           = { :git => 'https://github.com/vetri/SampleFreewheelObjCLib.git', :tag => s.version.to_s }

  s.ios.deployment_target = '10.0'

  s.source_files = 'SampleFreewheelObjCLib/Classes/**/*', 'SampleFreewheelObjCLib/Frameworks/AdManager.framework/Headers/*.h'
  s.public_header_files = 'SampleFreewheelObjCLib/Classes/**/*.h'
  s.vendored_frameworks = 'SampleFreewheelObjCLib/Frameworks/AdManager.framework'
  
  s.static_framework = true
end
